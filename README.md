A03 Adding server side functionality
My project(A03Vallamsetty) contains 4 web pages 1. index.html - It is the home page which introduces me. It contains video and image. 2. agecalculator.html - It takes inputs from the user for date of birth 3. contact.html - It is the contact page where users can enter their details,ask questions and submit which triggers a mail. 4. A simple guest book using Node, Express, BootStrap, EJS.
How to use
Open a command window in your c:\44563\A03 folder.
Run npm install to install all the dependencies in the package.json file.
Run node server.js to start the server. (Hit CTRL-C to stop.)
> npm install
> node server.js
Point your browser to http://localhost:8085.