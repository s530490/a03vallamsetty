QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 == "1", "1=1 success!");
});
QUnit.test("Testing the function testing_credits", function(assert)
{
    //assert.throws(function () {convertFeet(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
    assert.strictEqual(app.testing_credits(3,4,2),"valid inputs", 'Testing positive inputs');
    assert.throws(function () {app.testing_credits(null); }, /The given arguments is not a number/, 'Passing in null correctly raises an Error');
    assert.throws(function () {app.testing_credits(); }, /The given arguments is not a number/, 'Passing no arguments correctly raises an Error');
    assert.throws(function () {app.testing_credits(-2,-4,-5); },/The given numbers are negative/, 'Testing Negative Numbers');
    assert.throws(function () {app.testing_credits(2,4,-5); },/The given numbers are negative/, 'Testing Negative and positive Numbers');
    
});
QUnit.test('Testing our function day', function (assert) {
    assert.strictEqual(app.day(-10),-1, 'testing negative day value');
    assert.strictEqual(app.day(12),1,'testing positive day value');
    assert.strictEqual(app.day(0),0, 'testing for zero day value');
    
});
